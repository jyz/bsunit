package MagicTestsDemo

import (
	"errors"
	"fmt"
)

type NestedStruct struct {
	SomeStr       string
	SomeByte      byte
	SomeByteSlice []byte
}

type SomeStruct struct {
	SomeStr          string
	SomeInt          int64
	SomeNestedStruct *NestedStruct
}

func NewNestedStruct() *NestedStruct {
	return &NestedStruct{
		SomeStr:       "",
		SomeByte:      byte(0),
		SomeByteSlice: []byte{},
	}
}

func NewSomeStruct() *SomeStruct {
	return &SomeStruct{
		SomeStr:          "",
		SomeInt:          0,
		SomeNestedStruct: NewNestedStruct(),
	}
}

func (self *SomeStruct) DoSomething() (string, int64) {
	return fmt.Sprintf("SomeStr: %s", self.SomeStr), self.SomeInt + 1
}

func (self *SomeStruct) DoLoop() int64 {
	for i := 0; i < 5; i++ {
		self.SomeInt += 1
	}
	return self.SomeInt
}

func funcWithReturns() (string, int64) {
	return "1", 3
}

func funcWithParams(abc string, cd int64) (string, int64) {
	return abc + "1", cd + 3
}

// shortcut params, same type
func funcWithSameTypeParams(a, k string) string {
	return a + k
}

// return var is named
func funcWithNamedReturn(a, k string) (ret string) {
	ret = a + k
	return
}

// func with err as a return
func funcWithErrReturn(a, k string) (string, error) {
	return a + k, errors.New("Some error")
}
