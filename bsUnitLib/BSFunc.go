package BSUnit

import (
	"fmt"
	"strings"
)

type BSFunc struct {
	Name         string          // Name of func
	Instance     BSStructField   // (self *Symbol) <- this
	Params       []BSStructField // params a func takes in
	Returns      []BSStructField // returns, Name can be blank
	IsMethod     bool            // is it a function or method
	HasErrReturn bool            // does it have 1+ err as return
}

func NewBSFunc() BSFunc {
	return BSFunc{
		IsMethod:     false,
		HasErrReturn: false,
	}
}

func (self BSFunc) String() string {
	s := "[Func] "
	s = ConcatStrings(s, self.Name, "\n")
	s = ConcatStrings(s, "Instance: ", fmt.Sprint(self.Instance), "\n")
	s = ConcatStrings(s, "Params:\n")
	for _, p := range self.Params {
		s = ConcatStrings(s, " ", fmt.Sprint(p), "\n")
	}
	s = ConcatStrings(s, "Returns:\n")
	for _, r := range self.Returns {
		s = ConcatStrings(s, " ", fmt.Sprint(r), "\n")
	}
	s = ConcatStrings(s, "IsMethod: ", fmt.Sprintf("%v", self.IsMethod), "\n")
	s = ConcatStrings(s, "HasErrReturn: ", fmt.Sprintf("%v", self.HasErrReturn), "\n")
	return s
}

func (self *BSFunc) AddParam(name, typ string, isPointer bool) {
	// only create param if not exist

	for _, p := range self.Params {
		if p.Name == name {
			fmt.Println("Param already exist: ", name)
			return
		}
	}

	self.Params = append(self.Params, BSStructField{
		Name:         name,
		Type:         typ,
		IsPointer:    isPointer,
		DefaultValue: GenerateRandomVal(typ, isPointer),
	})
}

func (self *BSFunc) AddReturn(name, typ string, isPointer bool) {
	// if there's a name, loop through and make sure there's no dup
	if name != "" {
		for _, ret := range self.Returns {
			if ret.Name == name {
				fmt.Println("Return already exist: ", name)
				return
			}
		}
	}
	self.Returns = append(self.Returns, BSStructField{
		Name:         name,
		Type:         typ,
		IsPointer:    isPointer,
		DefaultValue: GenerateRandomExpectations(typ, isPointer),
	})

	if typ == "error" {
		self.HasErrReturn = true
	}
}

func (self *BSFunc) AddInstance(name, typ string, isPointer bool) {
	self.Instance = BSStructField{
		Name:         name,
		Type:         typ,
		IsPointer:    isPointer,
		DefaultValue: GenerateRandomVal(typ, isPointer),
	}
	self.IsMethod = true
}

func (self *BSFunc) CreateTestFunc() (funcStr string) {

	// Adding vars to func first
	funcStr = self.CreateFuncVars()
	hasErr := false
	errStr := ""              // for error var, we output test code string here
	var retVarStr string      // for each return var, we output test code string here
	var retVarTestsStr string // test code for return vars

	// check if instance function
	if !self.IsInstanceFunction() {

		// check if function has return var, if so we need to capture them
		if len(self.Returns) > 0 {
			// fmt.Println("got return vars")
			// var returnVars []string
			// for _, ret := range self.Returns {
			// 	if ret.Type == "error" {
			// 		if !hasErr {
			// 			returnVars = append(returnVars, "err")
			// 			hasErr = true
			// 		} else {
			// 			returnVars = append(returnVars, "_")
			// 		}
			// 	} else {
			// 		returnVars = append(returnVars, "_")
			// 	}
			// }
			// returnVarStr := strings.Join(returnVars, ",")
			retVarStr, retVarTestsStr, hasErr = self.CreateReturnVars()
			funcStr = ConcatStrings(funcStr, retVarStr, " := ")

		}
		funcStr = ConcatStrings(funcStr, self.Name+" (")
		if len(self.Params) > 0 {
			// fmt.Println("got param vars")
			var paramVars []string
			for _, paramVar := range self.Params {
				paramVars = append(paramVars, paramVar.Name)
			}
			paramVarStr := strings.Join(paramVars, ",")
			funcStr = ConcatStrings(funcStr, paramVarStr)
		}
		funcStr = ConcatStrings(funcStr, ")\n")
	} else {
		// if it IS instance func
		// i.e.
		//	func (self *Struct) funcName(){}

		funcStr = ConcatStrings(funcStr, self.CreateInstanceVar())

		// check if function has return var, if so we need to capture them
		if len(self.Returns) > 0 {
			// fmt.Println("got return vars")
			// var returnVars []string
			// for index, ret := range self.Returns {
			// 	if ret.Type == "error" {
			// 		if !hasErr {
			// 			returnVars = append(returnVars, "err")
			// 			hasErr = true
			// 		} else {
			// 			returnVars = append(returnVars, "err", string(index))
			// 		}
			// 	} else {
			// 		returnVars = append(returnVars, "_")
			// 	}
			// }
			// returnVarStr := strings.Join(returnVars, ",")
			retVarStr, retVarTestsStr, hasErr = self.CreateReturnVars()
			funcStr = ConcatStrings(funcStr, retVarStr, " := ")

		}
		funcStr = ConcatStrings(funcStr, self.Instance.Name, ".")
		funcStr = ConcatStrings(funcStr, self.Name+" (")
		if len(self.Params) > 0 {
			// fmt.Println("got param vars")
			var paramVars []string
			for _, paramVar := range self.Params {
				paramVars = append(paramVars, "param_"+paramVar.Name)
			}
			paramVarStr := strings.Join(paramVars, ",")
			funcStr = ConcatStrings(funcStr, paramVarStr)
		}
		funcStr = ConcatStrings(funcStr, ")\n")
	}

	if hasErr {

		// testcode for error var
		errStr = ConcatStrings(errStr, "\n\t// Error detected, logging it\n")
		errStr = ConcatStrings(errStr, "\tif err != nil {\n")
		errStr = ConcatStrings(errStr, "\t\tt.Log(err.Error())\n")
		errStr = ConcatStrings(errStr, "\t}\n")

		// adding err string to end
		funcStr = ConcatStrings(funcStr, errStr)
	}

	// funcStr = ConcatStrings(funcStr, "\tt.Log(\"pass\")\n")
	funcStr = ConcatStrings(funcStr, retVarTestsStr)
	return
}

/*
	Create func variables:
	* random params we need to pass in
	* returned errors
*/
func (self *BSFunc) CreateFuncVars() (funcStr string) {

	// extract all params
	// paramVars := make(map[string]string) // map[name]type
	if len(self.Params) > 0 {
		// fmt.Println("got param vars")
		for _, paramVar := range self.Params {
			// paramVars["param_"+paramVar.Name] = paramVar.Type
			funcStr = ConcatStrings(funcStr, paramVar.Name, " := ", paramVar.DefaultValue, "\n")
		}
	}

	// setting var_params
	// for name, typ := range paramVars {
	// 	funcStr = ConcatStrings(funcStr, name, " := ", GenerateRandomVal(typ, false), "\n")
	// }

	// extract all returns
	if len(self.Returns) > 0 {
		// fmt.Println("got ret vars")
		for _, retVar := range self.Returns {
			if retVar.Type == "error" {
				funcStr = ConcatStrings(funcStr, "var err error\n")
				break
			}
		}
	}

	return
}

/*
	Create return variables
*/
func (self *BSFunc) CreateReturnVars() (returnVarStr, retVarTestsStr string, hasErr bool) {
	hasErr = false

	// extract all params
	returnVars := []string{} // map[name]type

	for index, ret := range self.Returns {

		if ret.Type == "error" {
			if !hasErr {
				// returnVars = append(returnVars, "err")
				hasErr = true
			} else {
				// returnVars = append(returnVars, "err", string(index))
				// returnVars = append(returnVars, "_")

			}
		}
		retVar := ""
		if ret.Name != "" {
			retVar = ret.Name
			returnVars = append(returnVars, retVar)
		} else if ret.Type != "" {
			retVar = "return_" + ret.Type
			returnVars = append(returnVars, retVar)
		}

		// testcode for return var
		expectedValue := ret.DefaultValue
		retVarTestsStr = ConcatStrings(retVarTestsStr, fmt.Sprintf("\n\t// Testcode for var '%s'\n", retVar))
		retVarTestsStr = ConcatStrings(retVarTestsStr, fmt.Sprintf("\texpected%d := %s\n", index, expectedValue))
		retVarTestsStr = ConcatStrings(retVarTestsStr, fmt.Sprintf("\tif %s == expected%d {\n", retVar, index))
		retVarTestsStr = ConcatStrings(retVarTestsStr, "\t\tt.Log(\"Passed\")\n")
		retVarTestsStr = ConcatStrings(retVarTestsStr, "\t} else {\n")
		retVarTestsStr = ConcatStrings(retVarTestsStr, "\t\tt.Error(\"Failed\")\n")
		// retVarTestsStr = ConcatStrings(retVarTestsStr, fmt.Sprintf("\t\tt.Error(\"Expecting: %s\")\n", expectedValue))
		retVarTestsStr = ConcatStrings(retVarTestsStr, "\t}\n\n")

	}
	returnVarStr = strings.Join(returnVars, ",")

	return
}

func (self *BSFunc) CreateInstanceVar() (funcStr string) {

	if self.Instance.IsPointer {
		funcStr = "\n// Instance Var\n"
		funcStr = ConcatStrings(funcStr, self.Instance.Name, " := ")
		funcStr = ConcatStrings(funcStr, "&", self.Instance.Type, "{}\n")
	} else {
		funcStr = "\n// Instance Var\n"
		funcStr = ConcatStrings(funcStr, self.Instance.Name, " := ")
		funcStr = ConcatStrings(funcStr, self.Instance.Type, "{}\n")
	}
	return
}

func (self *BSFunc) IsInstanceFunction() bool {
	return self.Instance.Name != ""
}
