package BSUnit

import (
	"fmt"
	// "strings"
)

type BSProject struct {
	ProjectName string
	ClassDirMap map[string]string
	BSClasses   []BSClass
}

func NewBSProject() *BSProject {
	return &BSProject{
		ClassDirMap: map[string]string{},
	}
}

func (self *BSProject) String() string {
	s := "[Project] "
	s = ConcatStrings(s, self.ProjectName, "\n")

	s = ConcatStrings(s, "\n======Classes======\n")
	for _, c := range self.BSClasses {
		s = ConcatStrings(s, " ", fmt.Sprint(&c), "\n")
	}
	return s
}

// ================= CLASS ================= \\
func (self *BSProject) AddClassDir(class, dir string) {
	if self.ClassDirMap[class] != "" {
		fmt.Println("Class already has a dir")
		return
	}

	fmt.Printf("[AddClassDir] Adding \"%s\" with dir: %s\n", class, dir)
	self.ClassDirMap[class] = dir
}

func (self *BSProject) AddClass(newC BSClass) {
	fmt.Println("[add class]\n\n")
	fmt.Println(newC)

	// prune test funcs
	newC.PruneTestFuncs()
	fmt.Println("[after prune]\n\n")
	fmt.Println(newC)
	if len(self.BSClasses) < 1 {
		self.BSClasses = append(self.BSClasses, newC)
	} else {
		classExists := false
		for i, s := range self.BSClasses {
			if s.PackageName == newC.PackageName {
				classExists = true
				// fmt.Println("Package/class already exist: ", newC.PackageName)
				//TODO: attempt to merge
				fmt.Println("Merging class:", newC.PackageName)
				if len(s.Funcs) < 1 {
					s.Funcs = newC.Funcs
				} else {
					for _, newF := range newC.Funcs {
						doesExist := false
						for _, f := range s.Funcs {
							if f.Name == newF.Name {
								fmt.Println("Function exists, skipped", f.Name)
								doesExist = true
								break
							}
						}
						if !doesExist {
							s.AddFunc(newF)
						}
					}
				}
			}
			self.BSClasses[i] = s
		}
		if !classExists {
			self.BSClasses = append(self.BSClasses, newC)
		}
	}
}
