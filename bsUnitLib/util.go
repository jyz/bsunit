package BSUnit

import (
	"bytes"
	"crypto/rand"
	"fmt"
	"math/big"
	"strings"
)

func ConcatStrings(strings ...string) string {
	var buffer bytes.Buffer
	for _, s := range strings {
		buffer.WriteString(s)
	}
	return buffer.String()
}

func GetTestFileName(filename string) string {
	return ConcatStrings(filename[:len(filename)-3], "_test.go")
}

func GetFolderName(filename string) string {
	arr := strings.Split(filename, "/")
	fmt.Println(arr)
	if len(arr) <= 3 {
		return ""
	}
	return ConcatStrings(arr[0], "/", arr[1], "/", arr[2])
}

func RandFileName(root, language string) string {
	// foldername, _ := rand.Int(rand.Reader, big.NewInt(99999999999))
	filename, _ := rand.Int(rand.Reader, big.NewInt(99999999999))
	filePath := fmt.Sprintf("%s/%d.%s", root, filename, language)
	return filePath
}
