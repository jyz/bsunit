package BSUnit

import (
	"strings"
)

var bsTypes map[string]string // "type" : "sample declaration"

func GenerateRandomVal(typ string, isPointer bool) (s string) {
	lowerCaseType := strings.ToLower(typ)

	switch lowerCaseType {

	//error
	case "error":
		s = "*new(error)"
		break

	// string
	case "string":
		s = "\"placeholder\""
		break
	// byte
	case "byte":
		s = "byte{3}"
		break

	// rune
	case "rune":
		s = "rune{3}"
		break

	// uints
	case "uint64":
		s = "uint64(3)"
		break
	case "uint32":
		s = "uint32(3)"
		break
	case "uint16":
		s = "uint16(3)"
		break
	case "uint8":
		s = "uint8(3)"
		break

		// ints
	case "int64":
		s = "int64(3)"
		break
	case "int32":
		s = "int32(3)"
		break
	case "int16":
		s = "int16(3)"
		break
	case "int8":
		s = "int8(3)"
		break
	case "int":
		s = "int(3)"
		break

	// floats
	case "float64":
		s = "float64(3.0)"
		break
	case "float32":
		s = "float32(3.0)"
		break

	default:
		s = typ + "{}"
	}
	if isPointer {
		s = ConcatStrings("&", s)
	}
	return
}

func GenerateRandomExpectations(typ string, isPointer bool) (s string) {
	lowerCaseType := strings.ToLower(typ)

	switch lowerCaseType {

	//error
	case "error":
		s = "*new(error)"
		break

	// string
	case "string":
		s = "\"placeholder\""
		break

	// byte
	case "byte":
		s = "byte{3}"
		break

	// rune
	case "rune":
		s = "rune{3}"
		break

	// uints
	case "uint64":
		s = "uint64(3)"
		break
	case "uint32":
		s = "uint32(3)"
		break
	case "uint16":
		s = "uint16(3)"
		break
	case "uint8":
		s = "uint8(3)"
		break

		// ints
	case "int64":
		s = "int64(3)"
		break
	case "int32":
		s = "int32(3)"
		break
	case "int16":
		s = "int16(3)"
		break
	case "int8":
		s = "int8(3)"
		break
	case "int":
		s = "int(3)"
		break

	// floats
	case "float64":
		s = "float64(3.0)"
		break
	case "float32":
		s = "float32(3.0)"
		break

	default:
		s = ConcatStrings(typ, "{}")
	}

	if isPointer {
		s = ConcatStrings("&", s)
	}
	return
}
