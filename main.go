package main

import (
	"encoding/json"
	"fmt"
	"github.com/josephyzhou/bsUnit/bsUnitLib"
	"github.com/josephyzhou/bsUnit/parser"
	"io/ioutil"
	"os/exec"
)

func main() {

	// CreateTestFile("TestFile/someTestClass.go")
	// CreateTestFile("TestFileComplex/someTestClass.go")
	// parser.ParseDir("TestDir")
	// parser.ParseDir("vuvuzela")
	CreateTestDir("deferclient")
}

func CreateTestFile(filename string) {
	if parser.IsGoFile(filename) {
		testFileName := BSUnit.ConcatStrings(filename[:len(filename)-3], "_test.go")
		class := parser.ParseFile(filename)
		fmt.Println("=====Printing class=====\n", class)
		dataStr := class.CreateHeader()
		dataStr = BSUnit.ConcatStrings(dataStr, class.CreateTestFuncs())
		parser.ClearTestFileIfExist(testFileName)

		// write out test file
		ioutil.WriteFile(testFileName, []byte(dataStr), 0666)

		// write out json file
		CreateJsonFile(filename, class)
		// run go format
		cmd := exec.Command("go", "fmt", testFileName)
		o, err := cmd.Output()
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		fmt.Println(o)
	}
}

func CreateTestDir(dirName string) {
	project := parser.ParseDir(dirName)
	fmt.Println("=====Printing project=====\n", project)

	for _, class := range project.BSClasses {
		if class.PackageName == "main" {
			fmt.Println("Skipping main package, no test")
			break
		} else {
			filename := class.PackageName
			testFileName := BSUnit.ConcatStrings(filename[:len(filename)-3], "_test.go")
			classDir := project.ClassDirMap[filename]

			// after appending dir
			filename = BSUnit.ConcatStrings(classDir, "/", filename)
			testFileName = BSUnit.ConcatStrings(classDir, "/", testFileName)

			dataStr := class.CreateHeader()
			dataStr = BSUnit.ConcatStrings(dataStr, class.CreateTestFuncs())
			parser.ClearTestFileIfExist(classDir + testFileName)

			// write out test file
			ioutil.WriteFile(testFileName, []byte(dataStr), 0666)

			// write out json file
			CreateJsonFile(filename, &class)
		}

	}

}

func CreateJsonFile(filename string, class *BSUnit.BSClass) {
	jsonBytes, err := json.Marshal(class.Funcs)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	jsonFileName := BSUnit.ConcatStrings(filename[:len(filename)-3], ".json")
	ioutil.WriteFile(jsonFileName, jsonBytes, 0666)
}
