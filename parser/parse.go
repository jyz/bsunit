package parser

import (
	"fmt"
	"github.com/josephyzhou/bsUnit/bsUnitLib"
	"go/ast"
	"go/parser"
	"go/token"
	"golang.org/x/tools/go/types"
	"os"
)

func ClearTestFileIfExist(filename string) {
	_, err := os.Stat(filename)
	if err == nil {
		os.Remove(filename)
	}

}

func MakeDirForFile(filename string) {
	foldername := BSUnit.GetFolderName(filename)
	fmt.Println("Creating folder: ", foldername)
	if foldername != "" {
		err := os.Mkdir(foldername, 0666)
		if err != nil {
			fmt.Println(err.Error())
		}
	}
}

func RemoveDirForFile(filename string) {
	foldername := BSUnit.GetFolderName(filename)
	if foldername != "" {
		os.RemoveAll(foldername)
	}
}

func RemoveFilePair(filename, testfilename string) {
	_, err := os.Stat(filename)
	if err == nil {
		os.Remove(filename)
	} else {
		fmt.Println("filename:", filename, err.Error())
	}

	_, err = os.Stat(testfilename)
	if err == nil {
		os.Remove(testfilename)
	} else {
		fmt.Println("test filename:", testfilename, err.Error())
	}
}

func IsGoFile(filename string) bool {
	return filename[len(filename)-3:] == ".go"
}

func ParseFile(filename string) (class *BSUnit.BSClass) {
	class = BSUnit.NewBSClass()
	fset := token.NewFileSet() // positions are relative to fset
	f, err := parser.ParseFile(fset, filename, nil, parser.AllErrors)
	if err != nil {
		fmt.Println(err)
		return
	}
	// fmt.Println("\nScope\n======")
	// for _, obj := range f.Scope.Objects {
	// 	fmt.Println(obj.Name, obj.Kind)
	// 	if obj.Kind.String() == "type" {
	// 		newBSStruct := BSUnit.BSStruct{}
	// 		newBSStruct.Name = obj.Name
	// 		class.Structs = append(class.Structs, newBSStruct)
	// 	} else if obj.Kind.String() == "func" {
	// 		newBSFunc := new(BSUnit.BSFunc)
	// 		newBSFunc.Name = obj.Name
	// 		class.Funcs[obj.Name] = newBSFunc
	// 		//**  we can do this later
	// 	}
	// }

	// fmt.Println("\nPackage\n======")
	// fmt.Println(f.Name.String())
	class.PackageName = f.Name.String()

	// fmt.Println("\nImports\n======")
	for _, s := range f.Imports {
		// fmt.Println(s.Path.Value)
		// **this str already contains the quotation marks
		class.Imports = append(class.Imports, s.Path.Value)
	}

	// fmt.Println("\nAST Inspection\n======")

	ast.Inspect(f, func(n ast.Node) bool {
		switch x := n.(type) {
		case *ast.FuncDecl:
			newBSFunc := BSUnit.NewBSFunc()

			// fmt.Println("\n[Func]")
			// fmt.Println("Func name:", x.Name.String())
			newBSFunc.Name = x.Name.String()

			// Parsing Instance var for function
			if x.Recv != nil {
				// fmt.Print("Instance: ")
				for _, receive := range x.Recv.List {
					// fmt.Println(receive.Names, receive.Type)
					for _, instanceName := range receive.Names {
						instanceArr := HandleAstExpr(receive.Type)
						if len(instanceArr) != 1 {
							// fmt.Println("Should only get 1 instance, something's wrong")
							return false
						}

						instance := instanceArr[0]
						instance.Type, _ = ExprTypeString(receive.Type)
						instance.Name = instanceName.String()
						if instance.Name == "" {
							instance.Name = "instance"
						}
						newBSFunc.AddInstance(instanceName.String(), instance.Type, instance.IsPointer)

						// switch receive.Type.(type) {
						// case *ast.StarExpr:
						//  fmt.Println("star expr")
						//  fmt.Println(receive.Type.(*ast.StarExpr).X)
						//  newBSFunc.Instance = BSUnit.BSStructField{
						//    Type:      fmt.Sprint(receive.Type.(*ast.StarExpr).X),
						//    IsPointer: true,
						//  }
						// }
						// assert binary expr
						// newBSFunc.AddInstance(instanceName.String(), fmt.Sprint(receive.Type), false)
					}
				}
			}

			// Parsing params for a function
			// fmt.Println("Type: ")
			if x.Type != nil && x.Type.Params != nil {
				for pIndex, param := range x.Type.Params.List {
					// fmt.Println(" Params: ", param.Names, param.Type)
					for _, paramName := range param.Names {
						paramVar := HandleNestedAstExpr(fmt.Sprint(paramName), param.Type)
						paramVar.Type, _ = ExprTypeString(param.Type)
						newBSFunc.AddParam(paramVar.Name, paramVar.Type, paramVar.IsPointer)
					}

					// if none of the return statements have Name
					if len(param.Names) == 0 {
						// newBSFunc.AddReturn("", fmt.Sprint(result.Type), false)
						paramVar := HandleNestedAstExpr(BSUnit.ConcatStrings("param", fmt.Sprint(pIndex)), param.Type)
						paramVar.Type, _ = ExprTypeString(param.Type)
						newBSFunc.AddReturn(paramVar.Name, paramVar.Type, paramVar.IsPointer)
					}

				}
			}

			// Parsing returns for a function
			if x.Type != nil && x.Type.Results != nil {

				for rIndex, result := range x.Type.Results.List {
					// fmt.Println(" Returns: ", result.Names, result.Type)
					for _, retName := range result.Names {
						retVar := HandleNestedAstExpr(fmt.Sprint(retName), result.Type)
						retVar.Type, _ = ExprTypeString(result.Type)
						newBSFunc.AddReturn(retVar.Name, retVar.Type, retVar.IsPointer)

					}
					// if none of the return statements have Name
					if len(result.Names) == 0 {
						// newBSFunc.AddReturn("", fmt.Sprint(result.Type), false)
						retVar := HandleNestedAstExpr(BSUnit.ConcatStrings("return", fmt.Sprint(rIndex)), result.Type)
						retVar.Type, _ = ExprTypeString(result.Type)
						newBSFunc.AddReturn(retVar.Name, retVar.Type, retVar.IsPointer)
					}
				}
			}

			// add new BSFunc to map
			class.AddFunc(newBSFunc)

		// case *ast.StructType:
		//  fmt.Println(">>Using StructType:")
		//  fmt.Printf("\n[Struct]\n")
		//  for _, field := range x.Fields.List {
		//    fmt.Printf("names: %s, ", field.Names[0].Name)
		//    fmt.Printf("type: %s\n", field.Type)
		//  }
		case *ast.GenDecl:
			// fmt.Println(">>Using GenDecl:")
			if x.Tok.String() == "type" {
				// fmt.Printf("\n[Struct]\n")
				for _, spec := range x.Specs {
					class.AddStruct(HandleAstSpec(spec))
					// switch spec.(type) {
					// case *ast.TypeSpec:
					//  fmt.Println("TypeSpec detected:")
					//  fmt.Println("name:", spec.(*ast.TypeSpec).Name.String())
					//  fmt.Println("type:", spec.(*ast.TypeSpec).Type)
					//  switch spec.(*ast.TypeSpec).Type.(type) {
					//  case *ast.StructType:
					//    fmt.Println("StructType detected:")
					//    // fmt.Printf("\n[Struct]\n")
					//    for _, field := range spec.(*ast.TypeSpec).Type.(*ast.StructType).Fields.List {
					//      fmt.Printf("name: %s, ", field.Names[0].Name)
					//      fmt.Printf("type: %s\n", field.Type)
					//    }
					//  }
					// }
				}
			}
		}
		return true
	})

	return
}

func HandleAstSpec(spec ast.Spec) (structName string, bsSF []BSUnit.BSStructField) {
	switch spec.(type) {
	case *ast.TypeSpec:
		// fmt.Println("name:", spec.(*ast.TypeSpec).Name.String())
		// fmt.Println("type:", spec.(*ast.TypeSpec).Type)
		structName = spec.(*ast.TypeSpec).Name.String()
		bsSF = HandleAstExpr(spec.(*ast.TypeSpec).Type)
	}
	return
}

func HandleAstExpr(typ ast.Expr) (bsSF []BSUnit.BSStructField) {
	// fmt.Println("ast.Expr type:", types.ExprString(typ))

	switch typ.(type) {
	case *ast.StructType:
		// fmt.Println("Struct type")
		for _, field := range typ.(*ast.StructType).Fields.List {

			for _, n := range field.Names {
				// fmt.Printf("name: %s, ", n.Name)
				// fmt.Printf("type: %s\n", field.Type)

				switch field.Type.(type) {

				case *ast.StarExpr: // pointer case
				case *ast.ArrayType: // slice/array always here
					nestedSF := HandleNestedAstExpr(n.Name, field.Type)
					bsSF = append(bsSF, nestedSF)
					break
				default:
					bsSF = append(bsSF, BSUnit.BSStructField{
						Name: n.Name,
						Type: fmt.Sprint(field.Type),
					})
				}
				// bsSF = append(bsSF, BSUnit.BSStructField{
				//  Name: n.Name,
				//  Type: fmt.Sprint(field.Type),
				// })
			}

		}
		break

	case *ast.StarExpr:
		// fmt.Println("Star type")

		// fmt.Println(typ.(*ast.StarExpr).X)
		bsSF = append(bsSF, BSUnit.BSStructField{
			Type:      fmt.Sprint(typ.(*ast.StarExpr).X),
			IsPointer: true,
		})
		break

	case *ast.ArrayType:
		// fmt.Println("Array type")

		// fmt.Println("Slice: ", typ.(*ast.ArrayType).Elt, typ.(*ast.ArrayType).Len)
		bsSF = append(bsSF, BSUnit.BSStructField{
			Type: fmt.Sprint("[]", typ.(*ast.ArrayType).Elt),
		})
		break
	default:
		// fmt.Println("Default type")

		bsSF = append(bsSF, BSUnit.BSStructField{
			Type:      fmt.Sprint(typ),
			IsPointer: false,
		})
	}
	return
}

func HandleNestedAstExpr(name string, typ ast.Expr) (nestedSF BSUnit.BSStructField) {
	// fmt.Print("Name: ", name, " - ")
	innerSFArr := HandleAstExpr(typ)
	if len(innerSFArr) != 1 {
		fmt.Println("Should only get 1 nested obj, something's wrong")
		return
	}
	nestedSF = innerSFArr[0]
	nestedSF.Name = name
	return
}

// REFACTORING STARTS HERE =============
func ExprTypeString(typ ast.Expr) (typeStr string, isPointer bool) {
	typeStr = types.ExprString(typ)
	if typeStr[0] == '*' {
		// fmt.Println("First char is *, it's a pointer")
		typeStr = typeStr[1:]
	}
	return
}

// func GetStruct(name string, typ ast.Expr) (nestedSF BSUnit.BSStructField) {
// 	innerSFArr := HandleAstExpr(typ)
// 	if len(innerSFArr) != 1 {
// 		fmt.Println("Should only get 1 nested obj, something's wrong")
// 		return
// 	}
// 	nestedSF = innerSFArr[0]
// 	nestedSF.Name = name
// 	return
// }
