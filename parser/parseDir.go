package parser

import (
	"fmt"
	"github.com/josephyzhou/bsUnit/bsUnitLib"
	"go/ast"
	"go/parser"
	"go/token"
	"os"
	"path/filepath"
)

func ParseDir(parentDir string) (project *BSUnit.BSProject) {
	dirs := getAllDirSubdir(parentDir)
	project = BSUnit.NewBSProject()
	fmt.Println("cur dirs: ", dirs)
	for _, dir := range dirs {
		fmt.Println("cur dir: ", dir)
		fset := token.NewFileSet() // positions are relative to fset
		packages, err := parser.ParseDir(fset, dir, nil, parser.AllErrors)
		if err != nil {
			fmt.Println("error:", err)
			return
		}

		fmt.Println("packages len:", len(packages), "\n")

		// iterate over packages
		for _, curPackage := range packages {
			fmt.Printf("Package: %s\n==========\n", curPackage.Name)
			fmt.Println("files len:", len(curPackage.Files))

			project.AddClassDir(curPackage.Name, dir)
			// iterate over files within a package
			for _, f := range curPackage.Files {
				fmt.Printf("File: %s\n=====\n", f.Name, "\n")
				class := BSUnit.NewBSClass()
				class.PackageName = f.Name.String()

				ast.Inspect(f, func(n ast.Node) bool {
					switch x := n.(type) {
					case *ast.FuncDecl:
						newBSFunc := BSUnit.NewBSFunc()

						// fmt.Println("\n[Func]")
						// fmt.Println("Func name:", x.Name.String())

						newBSFunc.Name = x.Name.String()

						// Parsing Instance var for function
						if x.Recv != nil {
							// fmt.Print("Instance: ")
							for _, receive := range x.Recv.List {
								// fmt.Println(receive.Names, receive.Type)
								for _, instanceName := range receive.Names {
									instanceArr := HandleAstExpr(receive.Type)
									if len(instanceArr) != 1 {
										// fmt.Println("Should only get 1 instance, something's wrong")
										return false
									}

									instance := instanceArr[0]
									instance.Type, _ = ExprTypeString(receive.Type)
									if instance.Name == "" {
										instance.Name = "instance"
									}
									newBSFunc.AddInstance(instanceName.String(), instance.Type, instance.IsPointer)

									// switch receive.Type.(type) {
									// case *ast.StarExpr:
									//  fmt.Println("star expr")
									//  fmt.Println(receive.Type.(*ast.StarExpr).X)
									//  newBSFunc.Instance = BSUnit.BSStructField{
									//    Type:      fmt.Sprint(receive.Type.(*ast.StarExpr).X),
									//    IsPointer: true,
									//  }
									// }
									// assert binary expr
									// newBSFunc.AddInstance(instanceName.String(), fmt.Sprint(receive.Type), false)
								}
							}
						}

						// Parsing params for a function
						// fmt.Println("Type: ")
						if x.Type != nil && x.Type.Params != nil {
							for pIndex, param := range x.Type.Params.List {
								// fmt.Println(" Params: ", param.Names, param.Type)
								for _, paramName := range param.Names {
									paramVar := HandleNestedAstExpr(fmt.Sprint(paramName), param.Type)
									paramVar.Type, _ = ExprTypeString(param.Type)
									newBSFunc.AddParam(paramVar.Name, paramVar.Type, paramVar.IsPointer)
								}

								// if none of the return statements have Name
								if len(param.Names) == 0 {
									// newBSFunc.AddReturn("", fmt.Sprint(result.Type), false)
									paramVar := HandleNestedAstExpr(BSUnit.ConcatStrings("param", fmt.Sprint(pIndex)), param.Type)
									paramVar.Type, _ = ExprTypeString(param.Type)
									newBSFunc.AddReturn(paramVar.Name, paramVar.Type, paramVar.IsPointer)
								}

							}
						}

						// Parsing returns for a function
						if x.Type != nil && x.Type.Results != nil {
							for rIndex, result := range x.Type.Results.List {
								// fmt.Println(" Returns: ", result.Names, result.Type)
								for _, retName := range result.Names {
									retVar := HandleNestedAstExpr(fmt.Sprint(retName), result.Type)
									retVar.Type, _ = ExprTypeString(result.Type)
									newBSFunc.AddReturn(retVar.Name, retVar.Type, retVar.IsPointer)

								}
								// if none of the return statements have Name
								if len(result.Names) == 0 {
									// newBSFunc.AddReturn("", fmt.Sprint(result.Type), false)
									retVar := HandleNestedAstExpr(BSUnit.ConcatStrings("return", fmt.Sprint(rIndex)), result.Type)
									retVar.Type, _ = ExprTypeString(result.Type)
									newBSFunc.AddReturn(retVar.Name, retVar.Type, retVar.IsPointer)
								}
							}
						}

						// add new BSFunc to map
						class.AddFunc(newBSFunc)
						fmt.Println("adding a new func here")
						break
					// case *ast.StructType:
					//  fmt.Println(">>Using StructType:")
					//  fmt.Printf("\n[Struct]\n")
					//  for _, field := range x.Fields.List {
					//    fmt.Printf("names: %s, ", field.Names[0].Name)
					//    fmt.Printf("type: %s\n", field.Type)
					//  }
					case *ast.GenDecl:
						// fmt.Println(">>Using GenDecl:")
						if x.Tok.String() == "type" {
							// fmt.Printf("\n[Struct]\n")
							for _, spec := range x.Specs {
								class.AddStruct(HandleAstSpec(spec))
								// switch spec.(type) {
								// case *ast.TypeSpec:
								//  fmt.Println("TypeSpec detected:")
								//  fmt.Println("name:", spec.(*ast.TypeSpec).Name.String())
								//  fmt.Println("type:", spec.(*ast.TypeSpec).Type)
								//  switch spec.(*ast.TypeSpec).Type.(type) {
								//  case *ast.StructType:
								//    fmt.Println("StructType detected:")
								//    // fmt.Printf("\n[Struct]\n")
								//    for _, field := range spec.(*ast.TypeSpec).Type.(*ast.StructType).Fields.List {
								//      fmt.Printf("name: %s, ", field.Names[0].Name)
								//      fmt.Printf("type: %s\n", field.Type)
								//    }
								//  }
								// }
							}
						}
					}

					// add class to class list
					fmt.Println("Print class inside \n", class)
					project.AddClass(*class)
					return true
				})
				// return
			}

			// iterate over imports within the package
			for _, curImport := range curPackage.Imports {
				fmt.Printf("Import: %s, %v\n====\n", curImport.Name, curImport.Type)
			}
		}
	}
	fmt.Println("PRINT project:\n", project)
	return project
}

func getAllDirSubdir(parentDir string) (subdir []string) {

	// walk the dir tree
	subdir = []string{}
	// pathList := []string{}
	err := filepath.Walk(parentDir, func(path string, f os.FileInfo, err error) error {
		if f.IsDir() {

			subdir = append(subdir, path)
		}
		return nil
	})

	if err != nil {
		fmt.Println(err.Error())
	}

	for _, file := range subdir {
		fmt.Println(file)
	}

	return
}
